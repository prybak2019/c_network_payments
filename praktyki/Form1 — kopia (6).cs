﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace praktyki
{
    public partial class Form1 : Form
    {
        private static Socket ConnectSocket(string server, int port)
        {
            Socket s = null;
            IPHostEntry hostEntry = null;

            // Get host related information.
            hostEntry = Dns.GetHostEntry(server);

            // Loop through the AddressList to obtain the supported AddressFamily. This is to avoid
            // an exception that occurs when the host IP Address is not compatible with the address family
            // (typical in the IPv6 case).
            foreach (IPAddress address in hostEntry.AddressList)
            {
                IPEndPoint ipe = new IPEndPoint(address, port);
                Socket tempSocket =
                    new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                    s = tempSocket;
                    break;
                }
                else
                {
                    continue;
                }
            }
            return s;
        }

        private static string SocketSendReceive(string server, int port)
        {
            string request = "GET / HTTP/1.1\r\nHost: " + server +
                "\r\nConnection: Close\r\n\r\n";
            Byte[] bytesSent = Encoding.ASCII.GetBytes(request);
            Byte[] bytesReceived = new Byte[256];
            string page = "";

            // Create a socket connection with the specified server and port.
            using (Socket s = ConnectSocket(server, port))
            {

                if (s == null)
                    return ("Connection failed");

                // Send request to the server.
                s.Send(bytesSent, bytesSent.Length, 0);

                // Receive the server home page content.
                int bytes = 0;
                page = "Default HTML page on " + server + ":\r\n";

                // The following will block until the page is transmitted.
                do
                {
                    bytes = s.Receive(bytesReceived, bytesReceived.Length, 0);
                    page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
                }
                while (bytes > 0);
            }

            return page;
        }
        /*
        string result; // = SocketSendReceive("localhost", 8000);
        result = "<co:tak tak> \n <co:nie nie>";
            result = "The the <co:quick <co:brown fox  fox jumps over the lazy dog dog.";
            Match match = Regex.Match(result, @"<co:(?<word>\w+)\s+");
        //Regex rx = new Regex(@"<co:?<word>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        //rx = new Regex(@"\b(?<word>\w+)\s+(\k<word>)\b$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        //rx = new Regex(@"<co:(?<word>\w+)\s+", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        //MatchCollection matches = rx.Matches(result);
        //toolStripStatusLabel1.Text=matches.Count.ToString();
        result = "";
            while (match.Success)
            {
                result+=match.Groups["word"].Value+"\n";
                match = match.NextMatch();
            }
    richTextBox1.Text = result;*/

        public Form1()
        {
            InitializeComponent();
            minimiseAllBut();
        }

        private void saveCustomer()
        {
            var request = (HttpWebRequest)WebRequest.Create("http://localhost:8000/saveCustomer");

            var postData = "{";
            if (textBox4.Text != "") postData += "\"_id\":\"" + textBox4.Text + "\",";
            postData += "\"name\":\"" + textBox1.Text + "\",";
            postData += "\"surname\":\"" + textBox2.Text + "\",";
            postData += "\"email\":\"" + textBox3.Text + "\",";
            postData += "\"birthDate\":\"" + dateTimePicker1.Value.ToShortDateString() + "\"";
            postData += "}";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            responseString = responseString.Replace('"', '$');
            Match match = Regex.Match(responseString, @"\$_id\$:\$(?<_id>\w+)");
            if (match.Success)
            {
                textBox4.Text = match.Groups["_id"].Value;
            }
        }

        private string getCustomer()
        {
            string query = "http://localhost:8000/getCustomer";
            if (textBox1.Text != "") query += "/name=" + textBox1.Text;
            if (textBox2.Text != "") query += "/surname=" + textBox2.Text;
            if (textBox3.Text != "") query += "/email=" + textBox3.Text;
            if (dateTimePicker1.Enabled) query += "/birthDate=" + dateTimePicker1.Value.ToShortDateString();
            var request = (HttpWebRequest)WebRequest.Create(query);
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                toolStripStatusLabel1.Text = "Proszę wprowadź wartości we wskazane pola...";
                if (textBox1.Text == "") textBox1.BackColor = Color.LightGreen;
                if (textBox2.Text == "") textBox2.BackColor = Color.LightGreen;
                if (textBox3.Text == "") textBox3.BackColor = Color.LightGreen;
                return;
            }
            saveCustomer();
            if (getCustomer() == "[]")
            {
                toolStripStatusLabel1.Text = "Wystąpił błąd zapisu...";
            }
            else
            {
                toolStripStatusLabel1.Text = "Zapis zakończony powodzeniem...";
            };
            FillCheckedListBoxWithCustomers();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void FillCheckedListBoxWithCustomers()
        {
            checkedListBox1.Items.Clear();
            richTextBox1.Text = getCustomer();
            string responseString = getCustomer().Replace('"', '$');
            Match match = Regex.Match(responseString, @"\$_id\$:\$(?<_id>\w+)\$,\$name\$:\$(?<name>\w+)\$,\$surname\$:\$(?<surname>\w+)\$,\$email\$:\$(?<email>(\w|\.)+@(\w|\.)+)\$,\$birthDate\$:\$(?<birthDate>\w+-\w+-\w+)");
            while (match.Success)
            {
                checkedListBox1.Items.Add(match.Groups["name"].Value + " " + match.Groups["surname"].Value + " " + match.Groups["email"].Value + " " + match.Groups["birthDate"].Value + " // " + match.Groups["_id"].Value);
                match = match.NextMatch();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FillCheckedListBoxWithCustomers();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkedListBox1.SelectedIndex != -1) {
                textBox1.Enabled = textBox2.Enabled = textBox3.Enabled = textBox4.Enabled = dateTimePicker1.Enabled = true;
                string[] splitted = checkedListBox1.Items[checkedListBox1.SelectedIndex].ToString().Split(' ');
                textBox1.Text = splitted[0];
                textBox2.Text = splitted[1];
                textBox3.Text = splitted[2];
                textBox4.Text = splitted[5];
                string[] dateSplitted = splitted[3].Split('-');
                DateTime value = new DateTime(Int32.Parse(dateSplitted[0]), Int32.Parse(dateSplitted[1]), Int32.Parse(dateSplitted[2]));
                dateTimePicker1.Value = value;
            }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Enabled = !dateTimePicker1.Enabled;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox4.Text = "";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox4.Text = "5ca705475991be0d54fe39ca";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DateTime value = new DateTime(2019, 01, 23);
            dateTimePicker1.Value = value;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            dateTimePicker1.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = "Damian";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox2.Text = "Kowalski";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox3.Text = "qwerty@o2";
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            textBox1.BackColor = Color.White;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            textBox2.BackColor = Color.White;
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            textBox3.BackColor = Color.White;
        }

        private void checkedListBox1_Click(object sender, EventArgs e)
        {

        }
        private void databaseRemoveItem(string query) {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("http://localhost:8000/");
            HttpResponseMessage _response = _client.DeleteAsync(query).Result;
            if (_response.IsSuccessStatusCode)
            {
                toolStripStatusLabel1.Text = "Udało się usunąć klienta...";
            }
            else
                toolStripStatusLabel1.Text = "Niestety nie udało się usunąć klienta...";

        }
        private void button14_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count != 0) {
                string query = "removeCustomer";
                foreach (object itemChecked in checkedListBox1.CheckedItems)
                {
                    string[] splitted = itemChecked.ToString().Split(' ');
                    query += "/" + splitted[5];
                }
                databaseRemoveItem(query);
                FillCheckedListBoxWithCustomers();
            }

        }

        private void minimiseAllBut()
        {
            List<window> windows = common.allWindows();
            foreach (window Window in windows)
            {
                if (Window.name != this.Text)common.ShowWindow(Window.handle, 11);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            common.WNDtoBMP(button1.Handle,"out");
        }
    }
    public class window
    {
        public IntPtr handle;
        public String name;
        public String className;
        public List<window> childWindows=null;
    }
    public class common {
        private delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder strText, int maxCount);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);
        [DllImport("user32.dll")]
        private static extern bool IsWindowVisible(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumChildWindows(IntPtr hwndParent, EnumWindowsProc lpEnumFunc, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);
        [DllImport("User32.Dll")]
        private static extern void GetClassName(int hWnd, StringBuilder s, int nMaxCount);
        private static string _GetClassName(IntPtr hWnd)
        {
            StringBuilder sbClass = new StringBuilder(256);
            GetClassName((int)hWnd, sbClass, sbClass.Capacity);
            return sbClass.ToString();
        }
        private static bool EnumTheWindows(IntPtr hWnd, IntPtr listHandle)
        {
            List<window> windows = GCHandle.FromIntPtr(listHandle).Target as List<window>;
            int size = GetWindowTextLength(hWnd);
            if (size++ > 0 && IsWindowVisible(hWnd))
            {
                StringBuilder sb = new StringBuilder(size);
                GetWindowText(hWnd, sb, size);
                window Window = new window { handle=hWnd,name=sb.ToString(),className= _GetClassName(hWnd)};
                windows.Add(Window);
                IntPtr childWindow = GetWindow(hWnd, 5);//GW_CHILD = 5
                if (childWindow!=null&& IsWindowVisible(childWindow))
                {
                    Window.childWindows = new List<window>();
                    EnumChildWindows(hWnd, EnumTheWindows, GCHandle.ToIntPtr(GCHandle.Alloc(Window.childWindows)));
                };
            }
            return true;
        }
        public static List<window> allWindows()
        {
            List<window> windows = new List<window>();
            EnumWindows(EnumTheWindows, GCHandle.ToIntPtr(GCHandle.Alloc(windows)));
            return windows;
        }
        public static string windowsToString(List<window> windows, int progress)
        {
            string allWindows = "";
            foreach(window Window in windows)
            {
                for (int i = 0; i < progress; i++) allWindows += "  ";
                allWindows += Window.name + "     "+Window.className+"\n";
                if (Window.childWindows != null) allWindows += windowsToString(Window.childWindows,progress+1);
            }
            return allWindows;
        }
        [DllImport("User32.dll")]
        private static extern IntPtr GetDC(IntPtr hwnd);
        [DllImport("gdi32.dll", EntryPoint = "BitBlt", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt([In] IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, [In] IntPtr hdcSrc, int nXSrc, int nYSrc, uint dwRop);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetWindowRect(IntPtr hwnd, out Rectangle lpRect);
        public static void WNDtoBMP(IntPtr hwnd,string fName) {
            IntPtr wndDC = GetDC(hwnd);
            Graphics g1 = Graphics.FromHdc(wndDC);
            Rectangle rect;
            GetWindowRect(hwnd, out rect);
            int x = rect.Width- rect.X, y = rect.Height - rect.Y;
            Image myImage = new Bitmap(x, y, g1);
            Graphics g2 = Graphics.FromImage(myImage);
            IntPtr dc1 = g1.GetHdc();
            IntPtr dc2 = g2.GetHdc();
            BitBlt(dc2, 0, 0, x, y, dc1, 0, 0, 13369376);
            g1.ReleaseHdc(dc1);
            g2.ReleaseHdc(dc2);
            myImage.Save(fName+".bmp", ImageFormat.Bmp);
        }
        [DllImport("user32.dll", SetLastError = false)]
        static extern IntPtr GetDesktopWindow();
        public static void DesktopBMP(string fName)
        {
            WNDtoBMP(GetDesktopWindow(),fName);
        }
    }
}
